/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.commands;

import de.dbrag.wands.MagicWands;
import de.dbrag.wands.gui.Inventories;
import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.SpellColor;
import de.dbrag.wands.utils.Translator;
import de.dbrag.wands.utils.Updater;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author dbrosch
 */
public class WandCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        if (cs instanceof Player) {
            final Player p = (Player) cs;
            if (args.length == 0 || string.equalsIgnoreCase("wand")) {
                if (string.equalsIgnoreCase("wand")) {
                    if (p.getGameMode() == GameMode.CREATIVE) {
                        p.getInventory().addItem(MagicWands.wand(p));
                    } else {
                        ItemStack i = MagicWands.wand(p);
                        for (Spell s : MagicWands.values) {
                            if (p.hasPermission(s.getName())) {
                                MagicWands.addSpell(i, s);
                            }
                        }
                        p.getInventory().addItem(i);
                    }
                    //p.sendMessage("§c" + Translator.getText("custom.creative", "You must be in Creative Mode to use this", "Du musst im Kreativmodus sein, um diesen Befehl zu nutzen."));
                } else {
                    if (p.isOp()) {
                        p.openInventory(Inventories.settings_main(false, p));
                    } else {
                        p.sendMessage("§c" + Translator.getText("custom.nopermission", "I'm sorry, but you do not have permission to perform this command. Please contact the server administrator if you believe that this is in error.", "Leider können wir keinen Zugriff auf diesen Befehl gestatten, da die GEMA die erforderlichen Rechte nicht eingeräumt hat."));
                        return true;
                    }
                }
                return true;
            }
            if (args[0].equalsIgnoreCase("recipes")) {
                p.openInventory(Inventories.spells_recipe(false, p, 0));
            } else if (args[0].equalsIgnoreCase("give")) {
                if (p.getGameMode() == GameMode.CREATIVE) {
                    p.getInventory().addItem(MagicWands.wand(p));
                } else {
                    p.sendMessage("§c" + Translator.getText("custom.creative", "You must be in Creative Mode to use this", "Du musst im Kreativmodus sein, um diesen Befehl zu nutzen."));
                }
            } else {
                if (!p.isOp()) {
                    p.sendMessage("§c" + Translator.getText("custom.nopermission", "I'm sorry, but you do not have permission to perform this command. Please contact the server administrator if you believe that this is in error.", "Leider können wir keinen Zugriff auf diesen Befehl gestatten, da die GEMA die erforderlichen Rechte nicht eingeräumt hat."));
                    return true;
                }
                if (args[0].equalsIgnoreCase("update")) {
                    if (args.length == 1) {
                        Bukkit.getScheduler().runTaskAsynchronously(MagicWands.getInstance(), new Runnable() {

                            @Override
                            public void run() {
                                Updater u = new Updater(MagicWands.getInstance());
                                if (u.hasUpdate()) {
                                    u.downloadLatest(p);
                                } else {
                                    p.sendMessage("§a" + Translator.getText("custom.uptodate", "You are running the lastest version!", "Das Plugin ist aktuell."));
                                }
                            }
                        });
                    } else {
                        p.sendMessage("§cUsage: /magicwands update");
                    }
                } else if (args[0].equalsIgnoreCase("cfg")) {
                    if (args.length == 1) {
                        p.openInventory(Inventories.settings_settings(false, p));
                    } else {
                        p.sendMessage("§cUsage: /mw cfg");
                    }
                } else if (args[0].equalsIgnoreCase("help")) {
                    p.sendMessage("§9§lMagicWands §8Help");
                    p.sendMessage("§6/wand §8:§7 Gives you a wand");
                    p.sendMessage("§6/magicwands §8:§7 Opens the main interface");
                    p.sendMessage("§6/mw help §8:§7 Prints out this help message");
                    p.sendMessage("§6/mw addspell <spellid> [player]>");
                } else if (args[0].equalsIgnoreCase("addspell")) {
                    if (args.length < 2) {
                        p.sendMessage("§cUsage: /mw addspell <spellid> [player]");
                    } else {
                        if (args[1].equalsIgnoreCase("ALL")) {
                            p.sendMessage("§cPlease use the /wand command in Creative Mode for this until further notice.");
                        } else {
                            Spell s = null;
                            for (Spell spell : MagicWands.values) {
                                if (args[1].equalsIgnoreCase(spell.getName())) {
                                    s = spell;
                                    break;
                                }
                            }
                            if (s == null) {
                                p.sendMessage("§cInvalid Spell. Execute §6/mw recipes§c for a list of spells");
                            } else {
                                if (args.length == 3) {
                                    Player a = Bukkit.getPlayer(args[2]);
                                    if (a == null) {
                                        p.sendMessage("§cThis Player is not online.");
                                    } else {
                                        ItemStack[] contents = a.getInventory().getContents();
                                        for (ItemStack i : contents) {
                                            if (MagicWands.isWand(i)) {
                                                i = MagicWands.addSpell(i, s);
                                            }
                                        }
                                        a.getInventory().setContents(contents);
                                        p.sendMessage("§a" + a.getName() + " just learned a new Spell");
                                        a.sendMessage("§a" + Translator.getText("custom.newspell", "You just learned a new Spell", "Du hast gerade einen neuen Zauberspruch gelernt"));
                                    }
                                } else {
                                    ItemStack[] contents = p.getInventory().getContents();
                                    for (ItemStack i : contents) {
                                        if (MagicWands.isWand(i)) {
                                            i = MagicWands.addSpell(i, s);
                                        }
                                    }
                                    p.getInventory().setContents(contents);
                                    p.sendMessage("§a" + Translator.getText("custom.newspell", "You just learned a new Spell", "Du hast gerade einen neuen Zauberspruch gelernt"));
                                }
                            }
                        }
                    }
                } else if (args[0].equalsIgnoreCase("editspell") || args[0].equalsIgnoreCase("es")) {
                    if (args.length == 1) {
                        p.openInventory(Inventories.settings_spelledit(false, 0));
                    } else if (args.length >= 4) {
                        Spell s = null;
                        for (Spell sp : MagicWands.values) {
                            if (sp.getName().equalsIgnoreCase(args[1])) {
                                s = sp;
                                break;
                            }
                        }
                        if (s == null) {
                            p.sendMessage("§c" + Translator.getText("custom.invalidspell", "This Spell does not exist", "Dieser Spiel existiert nicht."));
                        } else {
                            if (args[2].equalsIgnoreCase("display") || args[2].equalsIgnoreCase("displayname")) {
                                StringBuilder b = new StringBuilder(args[3]);
                                for (int i = 4; i < args.length; i++) {
                                    b.append(" ");
                                    b.append(args[i]);
                                }
                                String display = b.toString();
                                s.d = display;
                                FileConfiguration cfg = MagicWands.getInstance().getConfig();
                                cfg.set(s.getName() + ".displayname", s.d);
                                MagicWands.getInstance().saveConfig();
                                p.sendMessage("§aYour changes have been saved.");
                                p.sendMessage("§c§lWarning§e§l The displaynames on the wands have not changed, so this Spell will not work on them anymore");
                            } else if (args[2].equalsIgnoreCase("color")) {
                                SpellColor c = SpellColor.fromString(args[3]);
                                if (c != null) {
                                    s.c = c;
                                    FileConfiguration cfg = MagicWands.getInstance().getConfig();
                                    cfg.set(s.getName() + ".color", s.c);
                                    MagicWands.getInstance().saveConfig();
                                    p.sendMessage("§aYour changes have been saved.");
                                } else {
                                    p.sendMessage("§cInvalid Color input. Please use the JSON format.");
                                }
                            } else if (args[2].equalsIgnoreCase("cost")) {
                                try {
                                    s.cost = Integer.parseInt(args[3]);
                                    FileConfiguration cfg = MagicWands.getInstance().getConfig();
                                    cfg.set(s.getName() + ".cost", s.cost);
                                    MagicWands.getInstance().saveConfig();
                                    p.sendMessage("§aYour changes have been saved.");
                                } catch (NumberFormatException ex) {
                                    p.sendMessage("§c" + ex.getClass().getName() + ": " + ex.getMessage());
                                }
                            } else {
                                if (s.hasMeta(args[2])) {
                                    int i = -404;
                                    try {
                                        i = Integer.parseInt(args[3]);
                                    } catch (NumberFormatException ex) {
                                        if(args[3].equalsIgnoreCase("true")){
                                            i = 1;
                                        }else if(args[3].equalsIgnoreCase("false")){
                                            i = 0;
                                        }else{
                                            p.sendMessage("§c" + ex.getClass().getName() + ": " + ex.getMessage());
                                        }
                                    }
                                    if(i==-404){
                                        return true;
                                    }
                                    s.setMeta(args[2], i);
                                    FileConfiguration cfg = MagicWands.getInstance().getConfig();
                                    cfg.set(s.getName() + "." + args[2], s.getMeta(args[2]));
                                    MagicWands.getInstance().saveConfig();
                                    p.sendMessage("§aYour changes have been saved.");
                                } else {
                                    p.sendMessage("§c" + "Invalid Property");
                                }
                            }
                        }

                    } else {
                        p.sendMessage("§cUsage: /magiwands editspell");
                    }
                } else {
                    p.sendMessage("§cThis subcommand has not been identified. Try '/wand help' for a list of commands");
                }
            }
        }
        return true;

    }

}
