/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the VHSDBN Network (vhsdbn.de) and currently developed by dbraggaming.
 */

package de.dbrag.wands.utils;

import de.dbrag.wands.MagicWands;
import java.text.MessageFormat;

/**
 * © Vhsdbn Network, 2015
 * 
 * You are not allowed to modify or edit this given code.
 * @author Dominik B. (dbraggaming)
 */
public class Translator {

    public static String getText(String id, String en){
        String lang = MagicWands.getInstance().getConfig().getString("language");
        if(lang.equalsIgnoreCase("custom")){
            String s = MagicWands.getInstance().getConfig().getString("id");
            if(s !=null){
                return s;
            }else{
                MagicWands.getInstance().getConfig().set(id, en);
                MagicWands.getInstance().saveConfig();
            }
        }
        return en;
    }
    
    public static String getText(String id, String en, String de){
        String lang = MagicWands.getInstance().getConfig().getString("language");
        if(lang.equalsIgnoreCase("custom")){
            String s = MagicWands.getInstance().getConfig().getString("id");
            if(s != null){
                return s;
            }else{
                MagicWands.getInstance().getConfig().set(id, en);
                MagicWands.getInstance().saveConfig();
            }
        }else if(lang.equalsIgnoreCase("de")){
            return de;
        }
        return en;
    }
    
    public static String getText(String id, String en, String de, Object[] params){
        String rs = null;
        String lang = MagicWands.getInstance().getConfig().getString("language");
        if(lang.equalsIgnoreCase("custom")){
            String s = MagicWands.getInstance().getConfig().getString("id");
            if(s !=null){
                rs =  s;
            }else{
                MagicWands.getInstance().getConfig().set(id, en);
                MagicWands.getInstance().saveConfig();
            }
        }else if(lang.equalsIgnoreCase("de")){
            rs = de;
        }
        if(rs==null){
            rs = en;
        }
        return MessageFormat.format(rs, params);
    }
    
    public static String getText(String id, String en, String de, String fr){
        String lang = MagicWands.getInstance().getConfig().getString("language");
        if(lang.equalsIgnoreCase("custom")){
            String s = MagicWands.getInstance().getConfig().getString("id");
            if(s !=null){
                return s;
            }else{
                MagicWands.getInstance().getConfig().set(id, en);
                MagicWands.getInstance().saveConfig();
            }
        }else if(lang.equalsIgnoreCase("de")){
            return de;
        }else if(lang.equalsIgnoreCase("fr")){
            return fr;
        }
        return en;
    }
    
}
