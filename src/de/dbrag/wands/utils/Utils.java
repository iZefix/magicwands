/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.utils;

import de.dbrag.wands.MagicWands;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.util.Vector;

/**
 *
 * @author dbraggaming
 */
public class Utils {

    public static Vector getRandomVector() {
        double x, y, z;
        x = Math.random() * 2 - 1;
        y = Math.random() * 2 - 1;
        z = Math.random() * 2 - 1;
        return new Vector(x, y, z).normalize();
    }

    public static void asyncOpenInventory(final Player p, final Inventory inv) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(MagicWands.getInstance(), new Runnable() {

            @Override
            public void run() {
                p.openInventory(inv);
            }
        }, 2L);
    }


    

    public static void sendMessage(Player p, String msg, ClickEvent e, String hover) {
        TextComponent c = new TextComponent(msg);
        c.setClickEvent(e);
        if (hover != null) {
            c.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hover).create()));
        }
        p.spigot().sendMessage(c);
    }


}
