/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the VHSDBN Network (vhsdbn.de) and currently developed by dbraggaming.
 */
package de.dbrag.wands.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.dbrag.wands.MagicWands;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

/**
 * © Vhsdbn Network, 2015
 *
 * You are not allowed to edit and share this given code.
 *
 * @author Dominik B. (dbraggaming)
 */
public class Updater {

    private final Plugin plugin;
    private String newVersion;
    private String newTitle;
    private String newDescription_en;
    private String currentVersion;
    private String nms;

    /**
     * Attention! All Calls are synchronized to the main Plugin.
     *
     * @param plugin
     */
    public Updater(Plugin plugin) {
        this.plugin = plugin;
        nms = Bukkit.getServer().getClass().getPackage().getName();
        nms = nms.substring(nms.lastIndexOf(".") + 1);
    }
    
    public Plugin getPlugin(){
        return plugin;
    }
    
    public String getNewVersion(){
        return newVersion;
    }
    
    public String getOldVersion(){
        return currentVersion;
    }
    
    public String getUpdateTitle(){
        return newTitle;
    }
    
    public String getUpdateDescription_en(){
        return newDescription_en;
    }
    
    public void downloadLatest(Player notifier){
        int preprogress = 0;
        int progress;
        try {
            File target = new File("plugins", plugin.getName());
            URL url = new URL("http://vhsdbn.de/plugin/"+plugin.getName()+"/latest");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("User-Agent", "PluginInstaller");
            int filesize = connection.getContentLength();
            float totalDataRead = 0;
            java.io.BufferedInputStream in = new java.io.BufferedInputStream(connection.getInputStream());
            java.io.FileOutputStream fos = new java.io.FileOutputStream(target);
            java.io.BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
            byte[] data = new byte[1024];
            int i = 0;
            while ((i = in.read(data, 0, 1024)) >= 0) {
                totalDataRead = totalDataRead + i;
                bout.write(data, 0, i);
                float Percent = (totalDataRead * 100) / filesize;
                progress = (int) Percent;
                if (preprogress != progress) {
                    MagicWands.sendActionBarMessage(notifier, "§6" + progress + "% §7- §8Downloading from " + url.getAuthority());
                    preprogress = progress;
                }
            }
            bout.close();
            in.close();
            MagicWands.sendActionBarMessage(notifier, "§aCOMPLETE - §8Saved to " + target.getPath());
            TextComponent c1 = new TextComponent("§7You have to reload the server to apply the Update. Reload now?");
            TextComponent c2 = new TextComponent("§a§l[YES]");
            c2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reload"));
            TextComponent c3 = new TextComponent("  §c§l[NO]");
            notifier.spigot().sendMessage(c1, c2, c3);
        } catch (Exception e) {
            MagicWands.sendActionBarMessage(notifier, "§cERROR §7- §8" + e.getClass().getName());
            notifier.sendMessage("§c" + e.getLocalizedMessage());
        }
    }

    public boolean hasUpdate() {
        try {
            PluginDescriptionFile f = plugin.getDescription();
            URL obj = new URL("https://api.vhsdbn.de/plugins/"+f.getName()+"/version");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setInstanceFollowRedirects(true);

            int responseCode = con.getResponseCode();
            System.out.println("["+plugin.getName()+"] Checking for updates");
            System.out.println("["+plugin.getName()+"] Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            inputLine = response.toString();
            if (inputLine == null) {
                throw new IOException("Could not read Data");
            }
            JsonElement element = new JsonParser().parse(inputLine);
            JsonObject object = element.getAsJsonObject();
            newVersion = object.get("version").getAsString();
            currentVersion = f.getVersion();
            if(!newVersion.equalsIgnoreCase(currentVersion)){
                newTitle = object.get("title").getAsString();
                newDescription_en = object.get("description_en").getAsString();
                System.out.println("["+plugin.getName()+"] An Update was found: v"+newVersion);
                return true;
            }
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    
    private void sendActionBarMessage(Player pl, String message) {
        try {
            
            Class<?> c1 = Class.forName("org.bukkit.craftbukkit." + nms + ".entity.CraftPlayer");
            Object p = c1.cast(pl);
            Object ppoc = null;
            Class<?> c4 = Class.forName("net.minecraft.server." + nms + ".PacketPlayOutChat");
            Class<?> c5 = Class.forName("net.minecraft.server." + nms + ".Packet");
            if (nms.equalsIgnoreCase("v1_8_R1") || !nms.startsWith("v1_8_")) {
                Class<?> c2 = Class.forName("net.minecraft.server." + nms + ".ChatSerializer");
                Class<?> c3 = Class.forName("net.minecraft.server." + nms + ".IChatBaseComponent");
                Method m3 = c2.getDeclaredMethod("a", new Class<?>[]{String.class});
                Object cbc = c3.cast(m3.invoke(c2, "{\"text\": \"" + message + "\"}"));
                ppoc = c4.getConstructor(new Class<?>[]{c3, byte.class}).newInstance(new Object[]{cbc, (byte) 2});
            } else {
                Class<?> c2 = Class.forName("net.minecraft.server." + nms + ".ChatComponentText");
                Class<?> c3 = Class.forName("net.minecraft.server." + nms + ".IChatBaseComponent");
                Object o = c2.getConstructor(new Class<?>[]{String.class}).newInstance(new Object[]{message});
                ppoc = c4.getConstructor(new Class<?>[]{c3, byte.class}).newInstance(new Object[]{o, (byte) 2});
            }
            Method m1 = c1.getDeclaredMethod("getHandle", new Class<?>[]{});
            Object h = m1.invoke(p);
            Field f1 = h.getClass().getDeclaredField("playerConnection");
            Object pc = f1.get(h);
            Method m5 = pc.getClass().getDeclaredMethod("sendPacket", new Class<?>[]{c5});
            m5.invoke(pc, ppoc);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (pl.isOp()) {
                pl.sendMessage("§cAn " + ex.getClass().getName() + " occured while sending a Packet. Please report the following to the Plugin Developer: §c" + ex.getLocalizedMessage());
            }
        }
    }


}
