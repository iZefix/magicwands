/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.utils;

import org.bukkit.inventory.ItemStack;

/**
 *
 * @author dbrosch
 */
public class CauldronRecipe {

    private ItemStack[] i;
    
    public CauldronRecipe(ItemStack[] ingredients){
        this.i = ingredients;
    }
    
    public ItemStack[] getIngredients(){
        return i;
    }
    
    public void setIngredients(ItemStack[] ingredients){
        i = ingredients;
    }
    
    
}
