/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class LevelThread implements Runnable {

    private static int task;

    public static void start(int tickperiod) {
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicWands.getInstance(), new LevelThread(), 20L, tickperiod);
    }

    public static void interrupt() {
        Bukkit.getScheduler().cancelTask(task);
    }

    @Override
    public void run() {
        if (MagicWands.getConfBoolean("paycooldown")) {
            for (Player a : Bukkit.getOnlinePlayers()) {
                if (MagicWands.levels.containsKey(a.getName())) {
                    if (MagicWands.levels.get(a.getName()) < 100) {
                        MagicWands.levels.put(a.getName(), MagicWands.levels.get(a.getName()) + 1);
                    }
                } else {
                    MagicWands.levels.put(a.getName(), 1);
                }
                if(MagicWands.isWand(a.getItemInHand())){
                    MagicWands.sendActionBarMessage(a, "§1"+MagicWands.levels.get(a.getName()));
                }
            }
        }
    }

}
