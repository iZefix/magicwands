/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands;

import static de.dbrag.wands.MagicWands.cauldrons;
import de.dbrag.wands.spells.UsedSpell;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author dbrosch
 */
public class CauldronThread implements Runnable {

    private static int task;

    public static void start(int tickperiod) {
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicWands.getInstance(), new CauldronThread(), 20L, tickperiod);
    }

    public static void interrupt() {
        Bukkit.getScheduler().cancelTask(task);
    }

    @Override
    public void run() {
        if (cauldrons.isEmpty()) {
            return;
        }
        for (final Location loc : cauldrons.keySet()) {
            if (loc.getBlock().getType() == Material.CAULDRON) {
                loc.getWorld().spigot().playEffect(loc.clone().add(0.5, 0.5, 0.5), Effect.FLYING_GLYPH, 0, 0, 1F, 1F, 1F, 1F, 10, 5);
            } else {
                for (ItemStack item : cauldrons.get(loc)) {
                    loc.getWorld().dropItemNaturally(loc, item);
                }
                cauldrons.remove(loc);
            }
        }
        for(UsedSpell s: MagicWands.spells.keySet()){
            int i = MagicWands.spells.get(s);
            if(i>1){
                MagicWands.spells.put(s, i-1);
            }else{
                MagicWands.spells.remove(s);
            }
        }
    }

}
