/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands;

import de.dbrag.wands.commands.WandCommand;
import de.dbrag.wands.listener.CraftingHandler;
import de.dbrag.wands.listener.SpellSelectHandler;
import de.dbrag.wands.listener.SpellUseHandler;
import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.UsedSpell;
import de.dbrag.wands.spells.presets.AvadaKedavra;
import de.dbrag.wands.spells.presets.Crucio;
import de.dbrag.wands.spells.presets.DefinedSpell;
import de.dbrag.wands.spells.presets.Fire;
import de.dbrag.wands.spells.presets.Fly;
import de.dbrag.wands.utils.Translator;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author dbrosch
 */
public class MagicWands extends JavaPlugin {

    private static MagicWands instance;
    public static ShapedRecipe r;
    public static boolean enabled = true;
    private static String nms;
    public static Material wand = Material.STICK;

    public static HashMap<Location, ArrayList<ItemStack>> cauldrons = new HashMap<>();
    public static HashMap<UsedSpell, Integer> spells = new HashMap<>();

    public static HashMap<String, Integer> levels = new HashMap<>();

    public static ArrayList<Spell> values = new ArrayList<>();

    public static MagicWands getInstance() {
        return instance;
    }

    public static boolean getConfBoolean(String path) {
        return instance.getConfig().getBoolean(path, false);
    }

    public static void setAPIonly(boolean apionly) {
        enabled = !apionly;
    }

    public static ItemStack addSpell(ItemStack wand, Spell spell) {
        ItemMeta m = wand.getItemMeta();
        List<String> lore = m.getLore();
        if (!lore.contains("§e" + spell.getDisplayname())) {
            if (lore.contains("§c" + Translator.getText("custom.nospelllore", "no spells available", "Keine Zaubersprüche verfügbar"))) {
                lore.remove("§c" + Translator.getText("custom.nospelllore", "no spells available", "Keine Zaubersprüche verfügbar"));
            }
            ArrayList<String> nlore = new ArrayList<>();

            for (String string : lore) {
                if (string.equalsIgnoreCase(" ")) {
                    break;
                } else {
                    nlore.add(string);
                }
            }
            nlore.add("§e" + spell.getDisplayname());
            m.setLore(nlore);
            wand.setItemMeta(m);
        }
        return wand;
    }

    public static ItemStack removeSpell(ItemStack wand, String displayname) {
        ItemMeta m = wand.getItemMeta();
        List<String> lore = m.getLore();
        if(lore.contains("§e"+displayname)){
            lore.remove("§e"+displayname);
        }
        m.setLore(lore);
        wand.setItemMeta(m);
        return wand;
    }
    /*
     int density = 10;
     double step = (2 * Math.PI) / density;
     double rad = 0.5;
     for (int i2 = 0; i2 < density; i2++) {
     double current = i2 * step;
     Location loc = new Location(c.getWorld(), c.getX() + rad * Math.cos(current), c.getY(), c.getZ() + rad * Math.sin(current));
     loc.getWorld().spigot().playEffect(loc, Effect.SMALL_SMOKE, 0, 0, 0F, 0F, 0F, 0F, 1, 20);
     }
     */

    public static ItemStack wand(Player owner) {
        ItemStack stack = new ItemStack(wand);
        ItemMeta stackMeta = stack.getItemMeta();
        if (owner == null) {
            return stack;
        } else {
            stackMeta.setDisplayName("§9§l" + owner.getName() + "'s " + Translator.getText("custom.wand", "Wand", "Zauberstab") + " §8(§c" + Translator.getText("custom.nospell", "No Spell", "Kein Zauberspruch") + "§8)");
            ArrayList<String> lore = new ArrayList<>();
            ArrayList<String> list = new ArrayList<>();
            if (owner.getGameMode() == GameMode.CREATIVE) {
                for (Spell spell : MagicWands.values) {
                    if (!list.contains(spell.getName())) {
                        list.add(spell.getName());
                    }
                }
            }
            if (!list.isEmpty()) {
                for (Spell spell : MagicWands.values) {
                    if (list.contains(spell.getName())) {
                        lore.add("§e" + spell.getDisplayname());
                    }
                }
            } else {
                lore.add("§c" + Translator.getText("custom.nospelllore", "no spells available", "Keine Zaubersprüche verfügbar"));
            }
            stackMeta.setLore(lore);
        }
        stack.setItemMeta(stackMeta);
        return stack;
    }

    public static boolean isWand(ItemStack stack) {
        if (stack == null || stack.getType() != wand) {
            return false;
        }
        String display = stack.getItemMeta().getDisplayName();
        return display.startsWith("§9§l") && display.contains("'s " + Translator.getText("custom.wand", "Wand", "Zauberstab") + " §8(");
    }

    @Override
    public void onEnable() {
        instance = this;

        Bukkit.getLogger().log(Level.INFO, "Bukkit version is {0}", Bukkit.getBukkitVersion());
        nms = Bukkit.getServer().getClass().getPackage().getName();
        nms = nms.substring(nms.lastIndexOf(".") + 1);

        enabled = getConfig().getBoolean("enabled", true);

        if (enabled) {
            r = new ShapedRecipe(wand(null)).shape("  C", " W ", "W  ").setIngredient('C', Material.NETHER_STAR).setIngredient('W', Material.WOOD);
            Bukkit.addRecipe(r);

            PluginManager pm = Bukkit.getPluginManager();
            pm.registerEvents(new SpellSelectHandler(), this);
            pm.registerEvents(new SpellUseHandler(), this);
            pm.registerEvents(new CraftingHandler(), this);
            Bukkit.getPluginCommand("wand").setExecutor(new WandCommand());

            CauldronThread.start(10);
            LevelThread.start(20);

            if (getConfig().contains("wandtype")) {
                Material m = Material.getMaterial(getConfig().getString("wandtype"));
                if (m == null) {
                    Bukkit.getLogger().log(Level.WARNING, "[MagicWands] Could not load {0}. Invalid Material.", getConfig().getString("wandtype"));
                } else if (m.isBlock()) {
                    Bukkit.getLogger().log(Level.WARNING, "[MagicWands] Could not load {0}. Material is a block!", getConfig().getString("wandtype"));
                } else {
                    wand = m;
                    Bukkit.getLogger().log(Level.INFO, "[MagicWands] Loaded {0} as default wand material. All old wands will not work any longer.", m.toString());
                }
            } else {
                getConfig().set("wandtype", wand.toString());
            }
            if (getConfig().contains("language")) {
                Bukkit.getLogger().log(Level.INFO, "[MagicWands] Using {0} as language", getConfig().getString("language"));
            } else {
                getConfig().set("language", "en");
                Bukkit.getLogger().log(Level.WARNING, "[MagicWands] Falling back to en_US since no language has been set.");
            }
            saveConfig();

            Bukkit.getLogger().log(Level.INFO, "[MagicWands] All main Functions are working properly");

            DefinedSpell.ACCIO.register();
            DefinedSpell.DIGGER.register();
            DefinedSpell.EXPELLIARMUS.register();
            DefinedSpell.FEATHER_FALL.register();
            DefinedSpell.FREEZER.register();
            DefinedSpell.LOVE.register();
            DefinedSpell.WINGARDIUM_LEVIOSA.register();

            new Fly().register();
            new Fire().register();
            new Crucio().register();
            new AvadaKedavra().register();

            Bukkit.getLogger().log(Level.INFO, "[MagicWands] Loaded {0} Spells.", values.size());
        }
    }

    public static void sendActionBarMessage(Player pl, String message) {
        try {
            Class<?> c1 = Class.forName("org.bukkit.craftbukkit." + nms + ".entity.CraftPlayer");
            Object p = c1.cast(pl);
            Object ppoc = null;
            Class<?> c4 = Class.forName("net.minecraft.server." + nms + ".PacketPlayOutChat");
            Class<?> c5 = Class.forName("net.minecraft.server." + nms + ".Packet");
            if (nms.equalsIgnoreCase("v1_8_R1") || !nms.startsWith("v1_8_")) {
                Class<?> c2 = Class.forName("net.minecraft.server." + nms + ".ChatSerializer");
                Class<?> c3 = Class.forName("net.minecraft.server." + nms + ".IChatBaseComponent");
                Method m3 = c2.getDeclaredMethod("a", new Class<?>[]{String.class});
                Object cbc = c3.cast(m3.invoke(c2, "{\"text\": \"" + message + "\"}"));
                ppoc = c4.getConstructor(new Class<?>[]{c3, byte.class}).newInstance(new Object[]{cbc, (byte) 2});
            } else {
                Class<?> c2 = Class.forName("net.minecraft.server." + nms + ".ChatComponentText");
                Class<?> c3 = Class.forName("net.minecraft.server." + nms + ".IChatBaseComponent");
                Object o = c2.getConstructor(new Class<?>[]{String.class}).newInstance(new Object[]{message});
                ppoc = c4.getConstructor(new Class<?>[]{c3, byte.class}).newInstance(new Object[]{o, (byte) 2});
            }
            Method m1 = c1.getDeclaredMethod("getHandle", new Class<?>[]{});
            Object h = m1.invoke(p);
            Field f1 = h.getClass().getDeclaredField("playerConnection");
            Object pc = f1.get(h);
            Method m5 = pc.getClass().getDeclaredMethod("sendPacket", new Class<?>[]{c5});
            m5.invoke(pc, ppoc);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (pl.isOp()) {
                pl.sendMessage("§cAn " + ex.getClass().getName() + " occured while sending a Packet. Please report the following to the Plugin Developer: §c" + ex.getLocalizedMessage());
            }
        }
    }

    @Override
    public void onDisable() {
        Bukkit.getScheduler().cancelTasks(this);
    }

}
