/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.gui;

import de.dbrag.wands.MagicWands;
import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.utils.Translator;
import de.dbrag.wands.utils.Updater;
import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.ChatPaginator;

/**
 *
 * @author dbrosch
 */
public class Inventories {

    public static ItemStack enabled(Player p) {
        ItemStack enabled = new ItemStack(Material.INK_SACK, 1, DyeColor.LIME.getDyeData());
        ItemMeta emeta = enabled.getItemMeta();
        emeta.setDisplayName(Translator.getText("custom.enabled", "§aEnabled", "§aAktiviert", "§aAllumé"));
        enabled.setItemMeta(emeta);
        return enabled;
    }

    public static ItemStack disabled(Player p) {
        ItemStack disabled = new ItemStack(Material.INK_SACK, 1, DyeColor.GRAY.getDyeData());
        ItemMeta dmeta = disabled.getItemMeta();
        dmeta.setDisplayName(Translator.getText("custom.disabled", "§7Disabled", "§7Deaktiviert", "§7Éteint"));
        disabled.setItemMeta(dmeta);
        return disabled;
    }

    public static ItemStack back() {
        ItemStack b = new ItemStack(Material.ARROW);
        ItemMeta bmeta = b.getItemMeta();
        bmeta.setDisplayName(Translator.getText("custom.back", "§c§lBack", "§c§lZurück", "§c§lRetour"));
        b.setItemMeta(bmeta);
        return b;
    }

    public static ItemStack next(int amount) {
        ItemStack b = new ItemStack(Material.STICK, amount);
        ItemMeta bmeta = b.getItemMeta();
        bmeta.setDisplayName(Translator.getText("custom.next", "§a§lNext Page", "§a§lNächste Seite"));
        b.setItemMeta(bmeta);
        return b;
    }

    public static ItemStack previous(int amount) {
        ItemStack b = new ItemStack(Material.STICK, amount);
        ItemMeta bmeta = b.getItemMeta();
        bmeta.setDisplayName(Translator.getText("custom.previous", "§a§lPrevious Page", "§a§lVorige Seite"));
        b.setItemMeta(bmeta);
        return b;
    }

    public static Inventory spells_recipe(boolean nameonly, Player p, int page) {
        Inventory inv = Bukkit.createInventory(null, 27, Translator.getText("custom.recipetitle", "§9§lAvailable Spells", "§9§lVerfügbare Zaubersprüche"));
        if (nameonly) {
            return inv;
        }
        int sslot = 18 * page;
        int slot = 0;
        for (Spell s : MagicWands.values) {
            if (sslot > 0) {
                sslot--;
                continue;
            } else if (slot == 18) {
                inv.setItem(23, next(page + 2));
                break;
            }
            ItemStack i = new ItemStack(Material.NETHER_STAR);
            ItemMeta iMeta = i.getItemMeta();
            iMeta.setDisplayName("§6§l" + s.getDisplayname());
            ArrayList<String> lore = new ArrayList<>();
            lore.add("§8" + s.getName());
            lore.add(" ");
            if (s.isCraftable()) {
                lore.add(Translator.getText("custom.recipematerials", "§7§nRequired Materials:", "§7§nBenötigte Materialien:", "§7§nMatériels"));
                for (ItemStack stack : s.getRecipe().getIngredients()) {
                    lore.add("§7" + stack.getAmount() + " " + stack.getType().name());
                }
            } else {
                lore.add("§c" + Translator.getText("custom.notcraftable", "Not craftable", "Nicht herstellbar", "Pas fabricable"));
            }
            iMeta.setLore(lore);
            i.setItemMeta(iMeta);
            inv.setItem(slot, i);
            slot++;
        }
        if (page > 0) {
            inv.setItem(21, previous(page));
        }
        int counter = 0;
        if (p.isOp()) {
            inv.setItem(22, back());
        }
        return inv;
    }

    public static Inventory spells_main(boolean nameonly, Player p) {
        Inventory inv = Bukkit.createInventory(null, 18, Translator.getText("custom.selectspell", "§9§lSelect Spell", "§9§lZauberspruch auswählen", "§9§lChoisit le formule magique"));
        if (nameonly) {
            return inv;
        }
        ArrayList<ItemStack> list = new ArrayList<>();
        for (String spell : p.getItemInHand().getItemMeta().getLore()) {
            if (spell.equalsIgnoreCase(" ")) {
                break;
            }
            spell = ChatColor.stripColor(spell);
            ItemStack s = new ItemStack(Material.NETHER_STAR);
            ItemMeta sMeta = s.getItemMeta();
            sMeta.setDisplayName(spell);
            s.setItemMeta(sMeta);
            list.add(s);
        }
        /**
         * 0 1 2 3 4 5 6 7 8
         * 9 0 1 2 3 4 5 6 7
         */
        if (list.size() == 1) {
            inv.setItem(4, list.get(0));
        } else if (list.size() == 2) {
            inv.setItem(3, list.get(0));
            inv.setItem(5, list.get(1));
        } else if (list.size() == 3) {
            inv.setItem(12, list.get(0));
            inv.setItem(4, list.get(1));
            inv.setItem(14, list.get(2));
        } else if (list.size() == 5) {
            inv.setItem(11, list.get(0));
            inv.setItem(12, list.get(1));
            inv.setItem(4, list.get(2));
            inv.setItem(14, list.get(3));
            inv.setItem(15, list.get(4));
        } else {
            int counter = 0;
            for (ItemStack s : list) {
                if (counter == 17) {
                    break;
                }
                inv.setItem(counter, s);
                counter++;
            }
        }
        return inv;
    }

    public static Inventory settings_main(boolean nameonly, Player p) {
        final Inventory inv = Bukkit.createInventory(null, 9, "§9MagicWands");
        if (nameonly) {
            return inv;
        }
        inv.setItem(1, MagicWands.wand(p));

        ItemStack settings = new ItemStack(Material.IRON_PICKAXE);
        ItemMeta sMeta = settings.getItemMeta();
        sMeta.setDisplayName("§8§l" + Translator.getText("custom.settings", "Settings", "Einstellungen", "Paramètres"));
        settings.setItemMeta(sMeta);
        inv.setItem(8, settings);

        ItemStack add = new ItemStack(Material.BOOK_AND_QUILL);
        ItemMeta addMeta = add.getItemMeta();
        addMeta.setDisplayName("§7§l" + Translator.getText("custom.editspells", "Edit Spells", "Zaubersprüche bearbeiten"));
        add.setItemMeta(addMeta);
        inv.setItem(3, add);

        ItemStack recipes = new ItemStack(Material.BOOK);
        ItemMeta rmeta = recipes.getItemMeta();
        rmeta.setDisplayName("§7§l" + Translator.getText("custom.recipes", "Recipes", "Rezepte", "Recettes"));
        recipes.setItemMeta(rmeta);
        inv.setItem(4, recipes);

        if (MagicWands.getInstance().getConfig().getBoolean("autocheck", true)) {
            Bukkit.getScheduler().runTaskAsynchronously(MagicWands.getInstance(), new Runnable() {

                @Override
                public void run() {
                    Updater u = new Updater(MagicWands.getInstance());
                    if (u.hasUpdate()) {
                        ItemStack item = new ItemStack(Material.REDSTONE_COMPARATOR);
                        ItemMeta iMeta = item.getItemMeta();
                        iMeta.setDisplayName("§6§l" + u.getUpdateTitle());
                        ArrayList<String> lore = new ArrayList<>();
                        lore.add("§8Version " + u.getNewVersion());
                        lore.add(" ");
                        for (String s : ChatPaginator.wordWrap(u.getUpdateDescription_en(), 40)) {
                            lore.add("§7" + s);
                        }
                        lore.add(" ");
                        lore.add("§a>> Download & Install Update");
                        iMeta.setLore(lore);
                        item.setItemMeta(iMeta);
                        inv.setItem(6, item);
                    }
                }
            });
        }

        ItemStack changelog = new ItemStack(Material.PAPER);
        ItemMeta changeMeta = changelog.getItemMeta();
        changeMeta.setDisplayName("§6§lChangelog");
        ArrayList<String> l = new ArrayList<>();
        l.add("§8Version " + MagicWands.getInstance().getDescription().getVersion());
        l.add(" ");
        for (String s : ChatPaginator.wordWrap(MagicWands.getInstance().getDescription().getDescription(), 40)) {
            l.add("§7" + s);
        }
        l.add(" ");
        l.add("§a>> Visit Plugin Page");
        changeMeta.setLore(l);
        changelog.setItemMeta(changeMeta);
        inv.setItem(7, changelog);

        return inv;
    }

    public static Inventory settings_settings(boolean nameonly, Player p) {
        Inventory inv = Bukkit.createInventory(null, 18, "§9MW §8" + Translator.getText("custom.settings", "Settings", "Einstellungen", "Paramètres"));
        if (nameonly) {
            return inv;
        }

        FileConfiguration cfg = MagicWands.getInstance().getConfig();

        /**
         * 0 1 2 3 4 5 6 7 8
         * 9 0 1 2 3 4 5 6 7
         */
        ItemStack exp = new ItemStack(Material.EXP_BOTTLE);
        ItemMeta expMeta = exp.getItemMeta();
        expMeta.setDisplayName("§6§lLevel Cost");
        expMeta.setLore(Arrays.asList("§7Firing a Spell will use up your", "§7experience. Experience will not regenerate."));
        exp.setItemMeta(expMeta);

        inv.setItem(0, exp);

        if (cfg.getBoolean("paylevel", true)) {
            inv.setItem(9, enabled(p));
        } else {
            inv.setItem(9, disabled(p));
        }

        // 1:10
        ItemStack language = new ItemStack(Material.BOOKSHELF);
        ItemMeta languageMeta = language.getItemMeta();
        languageMeta.setDisplayName("§6§lLanguage");
        languageMeta.setLore(Arrays.asList("§7This will change all common chat messages", "§7to the selected language."));
        language.setItemMeta(languageMeta);
        inv.setItem(1, language);

        ItemStack toChange = new ItemStack(Material.BOOK);
        ItemMeta tcMeta = toChange.getItemMeta();
        if (cfg.getString("language").contains("en")) {
            tcMeta.setDisplayName("§aEnglish");
        } else if (cfg.getString("language").contains("de")) {
            toChange.setAmount(2);
            tcMeta.setDisplayName("§aGerman §8(Deutsch)");
        } else if (cfg.getString("language").contains("fr")) {
            toChange.setAmount(4);
            tcMeta.setDisplayName("§aFrench §6§lBETA §8(Français)");
        } else if (cfg.getString("language").equalsIgnoreCase("custom")) {
            toChange.setAmount(3);
            tcMeta.setDisplayName("§aCustom §8(Edit in config.yml)");
        } else {
            toChange.setAmount(5);
        }
        toChange.setItemMeta(tcMeta);
        inv.setItem(10, toChange);

        ItemStack cooldown = new ItemStack(Material.WATCH);
        ItemMeta cooMeta = cooldown.getItemMeta();
        cooMeta.setDisplayName("§6§lCooldown");
        cooMeta.setLore(Arrays.asList("§7Adds an additional energy bar. It", "§7will regenerate by 1 Point every second."));
        cooldown.setItemMeta(cooMeta);
        inv.setItem(2, cooldown);
        if (cfg.getBoolean("paycooldown", false)) {
            inv.setItem(11, enabled(p));
        } else {
            inv.setItem(11, disabled(p));
        }

        ItemStack experimental = new ItemStack(Material.COMMAND);
        ItemMeta experimentalMeta = experimental.getItemMeta();
        experimentalMeta.setDisplayName("§6§lExperimental Features");
        experimentalMeta.setLore(Arrays.asList(ChatPaginator.wordWrap("§7Enables the SCS (Spell Colliding System) and other experimental features which §7use quite a lot of resources and are not fully §7tested yet.", 40)));
        experimental.setItemMeta(experimentalMeta);
        inv.setItem(6, experimental);
        if (cfg.getBoolean("experimental", false)) {
            inv.setItem(15, enabled(p));
        } else {
            inv.setItem(15, disabled(p));
        }

        ItemStack autocheck = new ItemStack(Material.REDSTONE_COMPARATOR);
        ItemMeta cmeta = autocheck.getItemMeta();
        cmeta.setDisplayName("§6§lAutocheck for Updates");
        cmeta.setLore(Arrays.asList("§7We are checking for Updates via our own ", "§7API. No Data is sent from this Plugin."));
        autocheck.setItemMeta(cmeta);
        inv.setItem(8, autocheck);
        if (cfg.getBoolean("autocheck", true)) {
            inv.setItem(17, enabled(p));
        } else {
            inv.setItem(17, disabled(p));
        }
        inv.setItem(13, back());
        //settings
        return inv;
    }

    public static Inventory settings_spelledit(boolean nameonly, int page) {
        Inventory inv = Bukkit.createInventory(null, 27, "§9§lMW §8" + Translator.getText("custom.titleeditspells", "Edit Spells", "Sprüche editieren"));
        if (nameonly) {
            return inv;
        }
        int sslot = 18 * page;
        int slot = 0;
        for (Spell s : MagicWands.values) {
            if (sslot > 0) {
                sslot--;
                continue;
            } else if (slot == 18) {
                inv.setItem(23, next(page + 2));
                break;
            }
            ItemStack i = new ItemStack(Material.NETHER_STAR);
            ItemMeta iMeta = i.getItemMeta();
            iMeta.setDisplayName("§6§l" + s.getDisplayname());
            ArrayList<String> lore = new ArrayList<>();
            lore.add("§8" + s.getName());
            lore.add(" ");
            if (s.c != null) {
                lore.add("§7Color Effect: §6" + s.c.getEffect().getName());
            } else {
                lore.add("§7Color Effect: §6None");
            }
            lore.add("§7Reach: §6" + s.r);
            lore.add("§7Range: §6" + s.h);
            lore.add("§7Cost: §6" + s.cost);
            for (String key : s.meta.keySet()) {
                lore.add("§7" + key + ": §6" + s.getMeta(key));
            }
            lore.add(" ");
            lore.add("§a>> Click to Edit");
            iMeta.setLore(lore);
            i.setItemMeta(iMeta);
            inv.setItem(slot, i);
            slot++;
        }
        if (page > 0) {
            inv.setItem(21, previous(page));
        }
        int counter = 0;
        inv.setItem(22, back());
        return inv;
    }

}
