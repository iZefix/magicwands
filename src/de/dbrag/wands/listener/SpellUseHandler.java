/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.listener;

import de.dbrag.wands.MagicWands;
import de.dbrag.wands.gui.Inventories;
import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.utils.Translator;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author dbrosch
 */
public class SpellUseHandler implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (!MagicWands.enabled) {
            return;
        }
        if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (e.getItem() == null) {
                return;
            } else if (MagicWands.isWand(e.getItem())) {
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock().getType().equals(Material.CAULDRON)) {
                    //with wand
                    if (MagicWands.cauldrons.containsKey(e.getClickedBlock().getLocation())) {
                        ArrayList<ItemStack> items = MagicWands.cauldrons.get(e.getClickedBlock().getLocation());
                        //e.getPlayer().sendMessage("§7Searching for suitable spells...");
                        for (Spell spell : MagicWands.values) {
                            if (spell.isCraftable()) {
                                //e.getPlayer().sendMessage("§7Checking " + s.getName() + "...");
                                ItemStack[] in = spell.getRecipe().getIngredients();
                                int i = 0;
                                for (ItemStack is1 : in) {
                                    boolean found = false;
                                    for (ItemStack is2 : items) {
                                        if (is1.isSimilar(is2)) {
                                            //e.getPlayer().sendMessage("§7Found Material." + is1.getType().toString());
                                            found = true;
                                            i++;
                                        }
                                    }
                                    if (!found) {
                                        //e.getPlayer().sendMessage("§7Failed to find Material." + is1.getType().toString() + ". Searching next spell...");
                                        break;
                                    }
                                }
                                if (i >= in.length) {
                                    //e.getPlayer().sendMessage("§aSuccessfully found matching spell " + s.getName());
                                    ItemStack item = e.getPlayer().getItemInHand();
                                    if (MagicWands.isWand(item)) {
                                        ItemMeta m = item.getItemMeta();
                                        List<String> lore = m.getLore();
                                        if (!lore.contains("§e" + spell.getDisplayname())) {
                                            e.getPlayer().setItemInHand(MagicWands.addSpell(item, spell));
                                            e.getPlayer().playSound(e.getClickedBlock().getLocation(), Sound.LEVEL_UP, 5, 2);
                                            e.getPlayer().sendMessage("§a"+Translator.getText("custom.newspell", "You just learned a new Spell", "Du hast gerade einen neuen Zauberspruch gelernt"));
                                            e.getPlayer().getWorld().spigot().strikeLightningEffect(e.getClickedBlock().getLocation().add(0, 1, 0), false);
                                            MagicWands.cauldrons.remove(e.getClickedBlock().getLocation());
                                        } else {
                                            e.getPlayer().sendMessage(Translator.getText("custom.spellalready", "§cYou already have this spell on your wand!", "§cDu hast diesen Zauberspruch bereits!"));
                                            return;
                                        }

                                    }
                                    return;
                                }
                            }
                        }
                        e.getPlayer().sendMessage("§c"+Translator.getText("custom.norecipe", "There has been no suitable crafting recipe", "Es wurde kein passendes Crafting-Rezept gefunden."));
                    }
                    return;
                }
                String displayname = e.getItem().getItemMeta().getDisplayName();
                displayname = displayname.split("§6")[1];
                displayname = displayname.substring(0, displayname.length() - 3);
                for (Spell spell : MagicWands.values) {
                    if (spell.getDisplayname().equalsIgnoreCase(displayname)) {
                        spell.fire(e.getPlayer());
                        break;
                    }
                }
            } else if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock().getType().equals(Material.CAULDRON)) {
                if (e.getClickedBlock().getData() == (byte) 3) {
                    //TODO fix nullpointer
                    if (!MagicWands.cauldrons.containsKey(e.getClickedBlock().getLocation())) {
                        MagicWands.cauldrons.put(e.getClickedBlock().getLocation(), new ArrayList<ItemStack>());
                    }
                    MagicWands.cauldrons.get(e.getClickedBlock().getLocation()).add(e.getItem());
                    e.getPlayer().setItemInHand(null);
                    e.setCancelled(true);
                } else if(e.getClickedBlock().getData() != (byte) 0) {
                    e.getPlayer().sendMessage(Translator.getText("custom.cauldronnotfull", "§cThe Cauldron must be full!", "§CDer Kessel muss voll sein!"));
                }
            }
        } else if (e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if (e.getItem() == null) {
                if (e.getAction() == Action.LEFT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.CAULDRON && MagicWands.cauldrons.containsKey(e.getClickedBlock().getLocation())) {
                    ArrayList<ItemStack> list = MagicWands.cauldrons.get(e.getClickedBlock().getLocation());
                    ItemStack s = list.get(list.size() - 1);
                    e.getClickedBlock().getWorld().dropItemNaturally(e.getClickedBlock().getLocation(), s);
                    list.remove(s);
                    if (list.isEmpty()) {
                        MagicWands.cauldrons.remove(e.getClickedBlock().getLocation());
                    } else {
                        MagicWands.cauldrons.put(e.getClickedBlock().getLocation(), list);
                    }
                }
            } else if (MagicWands.isWand(e.getItem())) {
                e.setCancelled(true);
                e.getPlayer().openInventory(Inventories.spells_main(false, e.getPlayer()));
            }
        }
    }

}
