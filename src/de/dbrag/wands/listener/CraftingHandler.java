/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.listener;

import de.dbrag.wands.MagicWands;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;

/**
 *
 * @author dbrosch
 */
public class CraftingHandler implements Listener{
    
    @EventHandler
    public void handleCraft(PrepareItemCraftEvent e){
        if(!MagicWands.enabled){
            return;
        }
        if(e.getInventory().getResult()!=null && e.getInventory().getResult().equals(MagicWands.wand(null))){
            e.getInventory().setResult(MagicWands.wand((Player)e.getView().getPlayer()));
            System.out.println("A new wand has been crafted by "+e.getView().getPlayer().getName());
        }
    }
}
