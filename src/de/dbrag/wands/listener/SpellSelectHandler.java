/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.listener;

import de.dbrag.wands.MagicWands;
import de.dbrag.wands.gui.Inventories;
import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.utils.Translator;
import de.dbrag.wands.utils.Utils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author dbrosch
 */
public class SpellSelectHandler implements Listener {

    @EventHandler
    public void handleInventoryClick(InventoryClickEvent e) {
        if (!MagicWands.enabled) {
            return;
        }
        final Player p = (Player) e.getWhoClicked();
        if (e.getInventory().getTitle().equalsIgnoreCase(Inventories.spells_main(true, null).getTitle())) {
            if (e.getCurrentItem().getType().equals(Material.NETHER_STAR)) {
                String displayname = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                for (Spell spell : MagicWands.values) {
                    if (spell.getDisplayname().equalsIgnoreCase(displayname)) {
                        ItemStack item = e.getWhoClicked().getItemInHand();
                        if (item != null && MagicWands.isWand(item)) {
                            ItemMeta iMeta = item.getItemMeta();
                            iMeta.setDisplayName("§9§l" + p.getName() + "'s " + Translator.getText("custom.wand", "Wand", "Zauberstab") + " §8(§6" + spell.getDisplayname() + "§8)");
                            item.setItemMeta(iMeta);
                            p.setItemInHand(item);
                        }
                        e.getView().close();
                        break;
                    }
                }
            }
            e.setCancelled(true);
            e.setResult(Event.Result.DENY);
            //TODO more checks
        } else if (e.getInventory().getTitle().equalsIgnoreCase(Inventories.settings_settings(true, p).getTitle())) {
            if (e.getRawSlot() == 9) {
                boolean b = false;
                if (e.getCurrentItem().equals(Inventories.enabled(p))) {
                    b = true;
                    e.setCurrentItem(Inventories.disabled(p));
                } else {
                    e.setCurrentItem(Inventories.enabled(p));
                }
                MagicWands.getInstance().getConfig().set("paylevel", !b);
                MagicWands.getInstance().saveConfig();
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            } else if (e.getRawSlot() == 11) {
                boolean b = false;
                if (e.getCurrentItem().equals(Inventories.enabled(p))) {
                    b = true;
                    e.setCurrentItem(Inventories.disabled(p));
                } else {
                    e.setCurrentItem(Inventories.enabled(p));
                }
                MagicWands.getInstance().getConfig().set("paycooldown", !b);
                MagicWands.getInstance().saveConfig();
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            } else if (e.getRawSlot() == 15) {
                boolean b = false;
                if (e.getCurrentItem().equals(Inventories.enabled(p))) {
                    b = true;
                    e.setCurrentItem(Inventories.disabled(p));
                } else {
                    e.setCurrentItem(Inventories.enabled(p));
                }
                MagicWands.getInstance().getConfig().set("experimental", !b);
                MagicWands.getInstance().saveConfig();
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            } else if (e.getRawSlot() == 17) {
                boolean b = false;
                if (e.getCurrentItem().equals(Inventories.enabled(p))) {
                    b = true;
                    e.setCurrentItem(Inventories.disabled(p));
                } else {
                    e.setCurrentItem(Inventories.enabled(p));
                }
                MagicWands.getInstance().getConfig().set("autocheck", !b);
                MagicWands.getInstance().saveConfig();
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            } else if (e.getRawSlot() == 13) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.settings_main(false, p));
            } else if (e.getRawSlot() == 10) {
                ItemStack toChange = new ItemStack(Material.BOOK);
                ItemMeta tcMeta = toChange.getItemMeta();
                if (e.getCurrentItem().getAmount() >= 4) {
                    tcMeta.setDisplayName("§aEnglish");
                    MagicWands.getInstance().getConfig().set("language", "en");
                } else if (e.getCurrentItem().getAmount() == 1) {
                    toChange.setAmount(2);
                    tcMeta.setDisplayName("§aGerman §8(Deutsch)");
                    MagicWands.getInstance().getConfig().set("language", "de");
                } else if (e.getCurrentItem().getAmount() == 2) {
                    toChange.setAmount(3);
                    tcMeta.setDisplayName("§aCustom §8(Edit in config.yml)");
                    MagicWands.getInstance().getConfig().set("language", "custom");
                } else if (e.getCurrentItem().getAmount() == 3) {
                    toChange.setAmount(4);
                    tcMeta.setDisplayName("§aFrench §6§lBETA §8(Français)");
                    MagicWands.getInstance().getConfig().set("language", "fr");
                }
                toChange.setItemMeta(tcMeta);
                e.setCurrentItem(toChange);
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                MagicWands.getInstance().saveConfig();
                Utils.asyncOpenInventory(p, Inventories.settings_settings(false, p));
            } else {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            }
        } else if (e.getInventory().getTitle().equals(Inventories.settings_main(true, p).getTitle())) {
            if (e.getCurrentItem().getType() == Material.PAPER) {
                e.getView().close();
                TextComponent c1 = new TextComponent("§7Click ");
                TextComponent c2 = new TextComponent("§6§nhere");
                c2.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://vhsdbn.de/plugin/magicwands"));
                TextComponent c3 = new TextComponent(" §7to visit the Plugin Page");
                p.spigot().sendMessage(c1, c2, c3);
            } else if (e.getCurrentItem().getType() == Material.IRON_PICKAXE) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.settings_settings(false, p));
            } else if (e.getCurrentItem().getType() == Material.BOOK) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.spells_recipe(false, p, 0));
            } else if (e.getCurrentItem().getType() == Material.REDSTONE_COMPARATOR) {
                e.getView().close();
                Bukkit.dispatchCommand(p, "mw update");
            } else if (e.getCurrentItem().getType() == Material.BOOK_AND_QUILL) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.settings_spelledit(false, 0));
            } else if (e.getSlot() == 1) {

            } else if (e.getClickedInventory() == null || e.getClickedInventory() != e.getInventory()) {

            } else {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            }
        } else if (e.getInventory().getTitle().equalsIgnoreCase(Inventories.spells_recipe(true, p, 0).getTitle())) {
            if (e.getCurrentItem().getType() == Material.STICK) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.spells_recipe(false, p, e.getCurrentItem().getAmount() - 1));
            } else if (e.getCurrentItem().getType() == Material.ARROW) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.settings_main(false, p));
            } else {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            }
        } else if (e.getInventory().getTitle().equalsIgnoreCase(Inventories.settings_spelledit(true, 0).getTitle())) {
            if (e.getCurrentItem().getType() == Material.NETHER_STAR) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Spell s = null;
                for (Spell spe : MagicWands.values) {
                    if (spe.getDisplayname().equalsIgnoreCase(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()))) {
                        s = spe;
                        break;
                    }
                }
                if (s == null) {
                    return;
                }
                e.getView().close();
                p.sendMessage("§9§lEdit Settings for " + s.getDisplayname() + " (" + s.getName() + ")");
                Utils.sendMessage(p, "§7• Change Displayname (String)", new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/mw es " + s.getName() + " displayname <newdisplay>"), "§a>> Click to Change the Displayname");
                Utils.sendMessage(p, "§7• Change Cost (Integer)", new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/mw es " + s.getName() + " cost <newcost>"), "§a>> Click to Change the Cost");
                Utils.sendMessage(p, "§7• Change Color (JSON-String, see Wiki)", new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/mw es " + s.getName() + " color <json-color>"), "§a>> Click to Change the Color");
                for (String key : s.meta.keySet()) {
                    Utils.sendMessage(p, "§7• Change " + key + " (Integer / Boolean)", new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/mw es " + s.getName() + " "+key+" <value>"), "§a>> Click to Change");
                }
            } else if (e.getCurrentItem().getType() == Material.STICK) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.spells_recipe(false, p, e.getCurrentItem().getAmount() - 1));
            } else if (e.getCurrentItem().getType() == Material.ARROW) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                Utils.asyncOpenInventory(p, Inventories.settings_main(false, p));
            } else {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
            }
        }
    }
}
