/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.spells;

import de.dbrag.wands.MagicWands;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 *
 * @author dbrosch
 */
public class UsedSpell {
    
    private final Player p;
    private final Spell s;
    private final Vector v;
    
    public UsedSpell(Player p, Spell s, Vector v){
        this.p = p;
        this.s = s;
        this.v = v;
    }
    
    public Player getPlayer(){
        return p;
    }
    
    public Spell getSpell(){
        return s;
    }
    
    public Vector getVector(){
        return v;
    }
    
    public UsedSpell getColliding(Location current, float hitrange){
        for(UsedSpell spell : MagicWands.spells.keySet()){
            if(spell.getPlayer()!=p){
                Location loc = spell.getPlayer().getLocation();
                loc = loc.add(0, 0.5, 0);
                for(int i = 0; i<40;i++){
                    loc = loc.add(v);
                    if(loc.distanceSquared(current)<hitrange*hitrange){
                        return spell;
                    }
                }
            }
        }
        return null;
    }
}