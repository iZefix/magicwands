/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the VHSDBN Network (vhsdbn.de) and currently developed by dbraggaming.
 */
package de.dbrag.wands.spells.presets;

import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.SpellColor;
import de.dbrag.wands.spells.UsedSpell;
import de.dbrag.wands.utils.CauldronRecipe;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * © Vhsdbn Network, 2015
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (dbraggaming)
 */
public class Fire extends Spell {

    /**
     * new Spell("spell.fire", "Fire", 40, 1F, SpellColor.FIRE) {
     *
     * @Override public boolean onEntityHit(Entity hit, Player shooter) { if
     * (hit instanceof Damageable) { Damageable d = (Damageable) hit;
     * d.setFireTicks(40);
     * shooter.getWorld().spigot().playEffect(hit.getLocation(),
     * Effect.LAVA_POP); return true; } return false; }
     *
     * @Override public boolean onBlockHit(Block b, Player shooter) {
     * if(b.getType()== Material.ICE){ b.setType(Material.STATIONARY_WATER); }
     * return b.getType().isSolid(); }
     *
     * @Override public void onFire(Player shooter) {
     *
     * }
     *
     * @Override public boolean onSpellHit(UsedSpell other, Location loc, Player
     * shooter) { return true; } }.setRecipe(new CauldronRecipe(new
     * ItemStack[]{new ItemStack(Material.FLINT), new
     * ItemStack(Material.IRON_INGOT), new ItemStack(Material.BLAZE_POWDER)}))
     * .setCost(2);
     */
    public Fire() {
        super("spell.fire", "Fire", 40, 1F, SpellColor.FIRE);
        setMeta("meltice", 1);
        setMeta("fireticks", 40);
        setCost(2);
        setRecipe(new CauldronRecipe(new ItemStack[]{new ItemStack(Material.FLINT), new ItemStack(Material.IRON_INGOT), new ItemStack(Material.BLAZE_POWDER)}));
    }

    @Override
    public boolean onEntityHit(Entity hit, Player shooter) {
        if (hit instanceof Damageable) {
            Damageable d = (Damageable) hit;
            d.setFireTicks(getMeta("fireticks"));
            shooter.getWorld().spigot().playEffect(hit.getLocation(), Effect.LAVA_POP);
            return true;
        }
        return false;
    }

    @Override
    public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
        return true;
    }

    @Override
    public boolean onBlockHit(Block b, Player shooter) {
        if (b.getType() == Material.ICE && getMeta("meltice") == 1) {
            b.setType(Material.STATIONARY_WATER);
        }
        return b.getType().isSolid();
    }

    @Override
    public void onFire(Player shooter) {
    }

}
