/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the VHSDBN Network (vhsdbn.de) and currently developed by dbraggaming.
 */

package de.dbrag.wands.spells.presets;

import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.SpellColor;
import de.dbrag.wands.spells.UsedSpell;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * © Vhsdbn Network, 2015
 * 
 * You are not allowed to modify or edit this given code.
 * @author Dominik B. (dbraggaming)
 */
public class Fly extends Spell{

    public Fly() {
        super("spell.fly", "Fly", 0, 0, SpellColor.BLUE);
        setCost(4);
        setMeta("multiplier", 3);
    }

    @Override
    public boolean onEntityHit(Entity hit, Player shooter) {
        return false;
    }

    @Override
    public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
        return false;
    }

    @Override
    public boolean onBlockHit(Block b, Player shooter) {
        return false;
    }

    @Override
    public void onFire(Player shooter) {
        shooter.setVelocity(shooter.getLocation().getDirection().multiply(getMeta("multiplier")));
    }

}
