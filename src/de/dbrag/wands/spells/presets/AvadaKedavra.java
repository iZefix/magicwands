/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the VHSDBN Network (vhsdbn.de) and currently developed by dbraggaming.
 */

package de.dbrag.wands.spells.presets;

import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.SpellColor;
import de.dbrag.wands.spells.UsedSpell;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * © Vhsdbn Network, 2015
 * 
 * You are not allowed to modify or edit this given code.
 * @author Dominik B. (dbraggaming)
 */
public class AvadaKedavra extends Spell{

    public AvadaKedavra() {
        super("spell.kill", "Avada Kedavra", 40, 0.7F, SpellColor.GREEN);
        this.setCost(20);
        this.setMeta("damage", 20);
    }

    @Override
    public boolean onEntityHit(Entity hit, Player shooter) {
        if(hit instanceof Damageable){
            Damageable damageable = (Damageable) hit;
            damageable.damage(this.getMeta("damage"), shooter);
            return true;
        }
        return false;
    }

    @Override
    public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
        return true;
    }

    @Override
    public boolean onBlockHit(Block b, Player shooter) {
        return b.getType().isSolid();
    }

    @Override
    public void onFire(Player shooter) {
    }

}
