/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.spells.presets;

import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.SpellColor;
import de.dbrag.wands.spells.UsedSpell;
import de.dbrag.wands.utils.CauldronRecipe;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

/**
 *
 * @author dbrosch
 */
public class DefinedSpell {



    public static Spell DIGGER = new Spell("spell.digg", "Digger", 40, 0, SpellColor.MAGENTA) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            return false;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            if (b.getType() != Material.AIR && !b.isLiquid() && b.getType() != Material.BEDROCK) {
                b.breakNaturally(new ItemStack(Material.DIAMOND_PICKAXE));
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void onFire(Player shooter) {

        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return false;
        }
    }.setRecipe(new CauldronRecipe(new ItemStack[]{new ItemStack(Material.DIAMOND_PICKAXE), new ItemStack(Material.DIAMOND_AXE), new ItemStack(Material.DIAMOND_SPADE), new ItemStack(Material.ENCHANTMENT_TABLE)}));

    public static Spell FREEZER = new Spell("spell.freeze", "Freezer", 10, 0, SpellColor.WATER) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            if (hit instanceof Creature) {
                Creature d = (Creature) hit;
                d.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 10, false, false), true);
                return true;
            }
            return false;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            if (b.getType().equals(Material.STATIONARY_WATER)) {
                b.setType(Material.ICE);
                return true;
            }
            return b.getType().isSolid();
        }

        @Override
        public void onFire(Player shooter) {
        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return true;
        }
    }.setRecipe(new CauldronRecipe(new ItemStack[]{new ItemStack(Material.ICE), new ItemStack(Material.OBSIDIAN)}));

    public static Spell EXPELLIARMUS = new Spell("spell.disarm", "Expelliarmus", 40, 1, SpellColor.RED) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            if (hit instanceof Player) {
                Player h = (Player) hit;
                if (h.getItemInHand() != null) {
                    ItemStack item = h.getItemInHand();
                    h.setItemInHand(null);
                    Item i = h.getWorld().dropItem(h.getLocation().add(0, 2, 0), item);
                    i.setVelocity(h.getLocation().getDirection().multiply(-1.5).setY(0.6));
                }
            }
            return false;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            return b.getType().isSolid();
        }

        @Override
        public void onFire(Player shooter) {

        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return true;
        }
    };
    public static Spell LOVE = new Spell("spell.love", "Love", 40, 1, SpellColor.HEART) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            if (hit instanceof Ageable) {
                Ageable a = (Ageable) hit;
                if(!a.isAdult()){
                    setCost(4);
                }else{
                    setCost(1);
                }
                a.setBreed(true);
            }
            return true;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            return b.getType().isSolid();
        }

        @Override
        public void onFire(Player shooter) {

        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return false;
        }
    };

    public static Spell FEATHER_FALL = new Spell("spell.featherfall", "Feather Fall", 0, 0, null) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            return false;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            return true;
        }

        @Override
        public void onFire(Player shooter) {
            shooter.setVelocity(new Vector(shooter.getVelocity().getX(), -0.1, shooter.getVelocity().getZ()));
            shooter.setFallDistance(0F);
            Location c = shooter.getLocation().subtract(0, 1, 0);
            c.getWorld().spigot().playEffect(c, Effect.CLOUD, 0, 0, 0F, 0F, 0F, 0F, 10, 40);
        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return false;
        }
    }.setRecipe(new CauldronRecipe(new ItemStack[]{new ItemStack(Material.FEATHER), new ItemStack(Material.DIAMOND_BOOTS), new ItemStack(Material.CARPET), new ItemStack(Material.SADDLE)}));

    public static Spell WINGARDIUM_LEVIOSA = new Spell("spell.moveup", "Wingardium Leviosa", 40, 0.7F, SpellColor.BLUE) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            hit.setVelocity(shooter.getLocation().getDirection().setY(1));
            return true;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            return b.getType().isSolid();
        }

        @Override
        public void onFire(Player shooter) {

        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return true;
        }

    }.setRecipe(new CauldronRecipe(new ItemStack[]{new ItemStack(Material.FEATHER), new ItemStack(Material.BLAZE_POWDER), new ItemStack(Material.CARPET)})).setCost(2);

    public static Spell ACCIO = new Spell("spell.move", "Accio", 40, 0.7F, SpellColor.YELLOW) {

        @Override
        public boolean onEntityHit(Entity hit, Player shooter) {
            hit.setVelocity(shooter.getLocation().getDirection().normalize().multiply(-hit.getLocation().distance(shooter.getLocation()) / 4));
            return false;
        }

        @Override
        public boolean onBlockHit(Block b, Player shooter) {
            return b.getType().isSolid();
        }

        @Override
        public void onFire(Player shooter) {

        }

        @Override
        public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
            return true;
        }
    }.setRecipe(new CauldronRecipe(new ItemStack[]{new ItemStack(Material.SLIME_BALL), new ItemStack(Material.BLAZE_POWDER), new ItemStack(Material.POTATO_ITEM)}));

}
