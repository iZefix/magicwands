/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.spells.presets;

import de.dbrag.wands.spells.Spell;
import de.dbrag.wands.spells.SpellColor;
import de.dbrag.wands.spells.UsedSpell;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author dbrosch
 */
public class Crucio extends Spell implements Listener{

    private static ArrayList<String> cruciod = new ArrayList<>();
    
    public Crucio() {
        super("spell.crucio", "Crucio", 20, 1, SpellColor.RED);
        this.setCost(10);
    }
    
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e){
        if(cruciod.contains(e.getPlayer().getName())){
            e.setTo(e.getFrom());
        }
    }

    @Override
    public boolean onEntityHit(Entity hit, Player shooter) {
        if(hit instanceof Player){
            Player h = (Player) hit;
            if(!h.getLocation().add(0, 4, 0).getBlock().getType().isSolid() && h.getLocation().subtract(0, 1, 0).getBlock().getType()!=Material.AIR){
                h.teleport(h.getLocation().add(0, 3, 0));
            }
            h.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20, 4, false, false), true);
            h.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20, 1, false, false), true);
            final String n = h.getName();
            cruciod.add(n);
            Bukkit.getScheduler().scheduleSyncDelayedTask(getProvidingPlugin(), new Runnable() {

                @Override
                public void run() {
                    cruciod.remove(n);
                }
            }, 20L);
            return true;
        }
        return false;
    }

    @Override
    public boolean onSpellHit(UsedSpell other, Location loc, Player shooter) {
        return true;
    }

    @Override
    public boolean onBlockHit(Block b, Player shooter) {
        return b.getType().isSolid();
    }

    @Override
    public void onFire(Player shooter) {
        
    }

}
