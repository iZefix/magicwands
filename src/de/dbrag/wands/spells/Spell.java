/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.spells;

import de.dbrag.wands.MagicWands;
import de.dbrag.wands.utils.CauldronRecipe;
import de.dbrag.wands.utils.ExperienceManager;
import de.dbrag.wands.utils.Translator;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author dbrosch
 */
public abstract class Spell {

    private final String n;
    public int r;
    public float h;
    public SpellColor c;
    public String d;
    private CauldronRecipe e;
    public int cost = 1;
    public HashMap<String, Integer> meta = new HashMap<>();

    /**
     * Constructs a new Spell with the given parameters
     *
     * @param name The config name of the spell
     * @param displayname The name to be displayed in the gui
     * @param reach The max length of the spell
     * @param hitrange The distance away from the spell to get damage
     * @param color The color of the spell
     */
    public Spell(String name, String displayname, int reach, float hitrange, SpellColor color) {
        n = name;
        r = reach;
        h = hitrange;
        c = color;
        d = displayname;
    }

    public String getName() {
        return n;
    }

    public String getDisplayname() {
        return d;
    }

    public int getCost() {
        return cost;
    }

    public Plugin getProvidingPlugin() {
        return MagicWands.getInstance();
    }

    /**
     * Registers the Spell for the Plugin
     */
    public void register() {
        if (this instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) this, getProvidingPlugin());
        }
        FileConfiguration cfg = MagicWands.getInstance().getConfig();
        if(cfg.contains(getName())){
            //Parameters: cost, reach, hitrange, color, displayname
            if(cfg.contains(getName()+".color")){
                SpellColor c = SpellColor.fromString(cfg.getString(getName()+".color"));
                if(c!=null){
                    this.c = c;
                }
            }
            if(cfg.contains(getName()+".cost")){
                int i = cfg.getInt(getName()+".cost");
                if(i!=0){
                    this.cost = i;
                }
            }
            if(cfg.contains(getName()+".displayname")){
                this.d = cfg.getString(getName()+".displayname");
            }
            for(String key : meta.keySet()){
                if(cfg.contains(getName()+"."+key)){
                    setMeta(key, cfg.getInt(getName()+"."+key));
                }
            }
        }
        MagicWands.values.add(this);
    }
    
    public void setMeta(String key, int value){
        meta.put(key, value);
    }
    
    public boolean hasMeta(String key){
        return meta.containsKey(key);
    }
    
    public int getMeta(String key){
        return meta.get(key);
    }

    public void fire(Player shooter) {
        if (MagicWands.getInstance().getConfig().getBoolean("paylevel", true)) {
            if (shooter.getGameMode() != GameMode.CREATIVE) {
                ExperienceManager manager = new ExperienceManager(shooter);
                if (manager.hasExp(cost)) {
                    manager.changeExp(-cost);
                } else {
                    MagicWands.sendActionBarMessage(shooter, "§c"+Translator.getText("custom.noenergy", "Not Enough Energy", "Nicht genug Energie"));
                    shooter.playSound(shooter.getLocation(), Sound.WOOD_CLICK, 3, 0.5F);
                    return;
                }
            }
        }
        if (MagicWands.getInstance().getConfig().getBoolean("paycooldown", false)) {
            if (MagicWands.levels.containsKey(shooter.getName()) && MagicWands.levels.get(shooter.getName()) > getCost()) {
                MagicWands.levels.put(shooter.getName(), MagicWands.levels.get(shooter.getName()) - cost);
                MagicWands.sendActionBarMessage(shooter, "§1" + MagicWands.levels.get(shooter.getName()));
            } else {
                MagicWands.sendActionBarMessage(shooter, "§c"+Translator.getText("custom.noenergy", "Not Enough Energy", "Nicht genug Energie"));
                return;
            }
        }
        
        onFire(shooter);
        final UsedSpell s = new UsedSpell(shooter, this, shooter.getLocation().getDirection());
        MagicWands.spells.put(s, 4);
        if (r > 0) {
            Location loc = shooter.getLocation().add(0, 0.8, 0);
            for (int i = 0; i < r * 2; i++) {
                loc.add(shooter.getLocation().getDirection().multiply(0.5F));
                if (MagicWands.getConfBoolean("experimental")) {
                    UsedSpell u = s.getColliding(loc, h*h*2);
                    if (u != null) {
                        if (onSpellHit(u, loc, shooter)) {
                            loc.getWorld().spigot().playEffect(loc.clone().add(0, 2, 0), Effect.EXPLOSION_LARGE, 0, 0, 0F, 2F, 0F, 1F, 4, 20);
                            return;
                        }
                    }
                }
                for (Entity en : loc.getBlock().getChunk().getEntities()) {
                    if (en.getLocation().distanceSquared(loc) <= (h * h)) {
                        if (!en.equals(shooter)) {
                            if (onEntityHit(en, shooter)) {
                                return;
                            }
                        }
                    }
                }
                if (onBlockHit(loc.getBlock(), shooter)) {
                    loc.getWorld().spigot().playEffect(loc, Effect.CLOUD, 0, 0, 0F, 0F, 0F, 0F, 4, 20);
                    return;
                } else {
                    /*
                    public void playEffect(Location location, Effect effect, int id, int data, float offsetX, float offsetY, float offsetZ, float speed, int particleCount, int radius)
                    */
                    if (c != null) {
                        loc.getWorld().spigot().playEffect(loc, c.getEffect(), c.getID(), c.getData(), c.getX(), c.getY(), c.getZ(), c.getSpeed(), 1, 20);
                    }
                }

            }
        }
    }

    /**
     * Handles the hit of a spell
     *
     * @param hit The entity
     * @param shooter The shooter
     * @return true: spell blocked false: spell continues
     */
    public abstract boolean onEntityHit(Entity hit, Player shooter);

    /**
     * Called when the spell hits another one
     *
     * @param other The other spell
     * @param loc The location
     * @param shooter The shooter
     * @return true: blocked
     */
    public abstract boolean onSpellHit(UsedSpell other, Location loc, Player shooter);

    /**
     * Called when the spell hits a block
     *
     * @param b the Block
     * @param shooter the shooter
     * @return true: isblocked
     */
    public abstract boolean onBlockHit(Block b, Player shooter);

    /**
     * Called when the spell is fired
     *
     * @param shooter The shooter
     */
    public abstract void onFire(Player shooter);

    public CauldronRecipe getRecipe() {
        return e;
    }

    public boolean isCraftable() {
        return e != null;
    }

    /**
     * Sets the recipe of this spell
     *
     * @param recipe
     * @return the same Spell for chaining
     */
    public Spell setRecipe(CauldronRecipe recipe) {
        e = recipe;
        return this;
    }

    /**
     * Sets the cost of this spell
     *
     * @param cost
     * @return the same Spell for chaining
     */
    public Spell setCost(int cost) {
        this.cost = cost;
        return this;
    }

}
