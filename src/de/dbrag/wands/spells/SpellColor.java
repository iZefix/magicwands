/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dbrag.wands.spells;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.bukkit.Effect;
import org.bukkit.Material;

/**
 *
 * @author dbrosch
 */
public class SpellColor {

    public static SpellColor BLUE = new SpellColor(Effect.TILE_DUST, Material.LAPIS_BLOCK.getId(), 0);
    public static SpellColor RED = new SpellColor(Effect.TILE_DUST, Material.REDSTONE_BLOCK.getId(), 0);
    public static SpellColor GREEN = new SpellColor(Effect.TILE_DUST, Material.EMERALD_BLOCK.getId(), 0);
    public static SpellColor YELLOW = new SpellColor(Effect.TILE_DUST, Material.GOLD_BLOCK.getId(), 0);
    public static SpellColor MAGENTA = new SpellColor(Effect.TILE_DUST, Material.WOOL.getId(), 2);
    public static SpellColor FIRE = new SpellColor(Effect.FLAME, 0, 0);
    public static SpellColor WATER = new SpellColor(Effect.SPLASH, 0, 0);
    public static SpellColor HEART = new SpellColor(Effect.HEART, 0, 0);

    private final Effect e;
    private int i = 0;
    private int d = 0;
    private float x = 0;
    private float y = 0;
    private float z = 0;
    private float s = 0;

    public SpellColor(Effect effect) {
        this.e = effect;
    }

    public static  SpellColor fromString(String input) {
        JsonElement element = new JsonParser().parse(input);
        JsonObject object = element.getAsJsonObject();
        if(object.get("e")==null){
            return null;
        }
        Effect ef = Effect.valueOf(object.get("e").getAsString());
        if(ef==null){
            return null;
        }
        SpellColor c = new SpellColor(ef);
        if(object.get("i")!=null){
            c.i = object.get("i").getAsInt();
        }
        if(object.get("d")!=null){
            c.d = object.get("d").getAsInt();
        }
        if(object.get("x")!=null){
            c.x = object.get("x").getAsFloat();
        }
        if(object.get("y")!=null){
            c.y = object.get("y").getAsFloat();
        }
        if(object.get("z")!=null){
            c.z = object.get("z").getAsFloat();
        }
        
        if(object.get("s")!=null){
            c.s = object.get("s").getAsFloat();
        }
        return c;
    }

    public SpellColor(Effect effect, float xcolor, float ycolor, float zcolor) {
        this.e = effect;
        this.x = xcolor;
        this.y = ycolor;
        this.z = zcolor;
        this.s = 1;

    }

    public SpellColor(Effect effect, int id, int data) {
        this.e = effect;
        this.i = id;
        this.d = data;
        this.s = 0;
    }
    
    @Override
    public String toString(){
        return new Gson().toJson(this);
    }

    public Effect getEffect() {
        return e;
    }

    public int getID() {
        return i;
    }

    public int getData() {
        return d;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public float getSpeed() {
        return s;
    }

}
